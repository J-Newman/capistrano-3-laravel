<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

	<title></title>
	<link rel="stylesheet" type="text/css" media="screen" href="{{ URL::asset('assets/stylesheets/styles.css') }}" />
	<script type="text/javascript" src="{{ URL::asset('assets/javascripts/jquery-1.11.2.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/javascripts/bootstrap.min.js') }}"></script>
	
	<!--[if IE]>
		<script type="text/javascript" src="{{ URL::asset('assets/javascripts/shiv.js') }}"></script>
		<script type="text/javascript" src="{{ URL::asset('assets/javascripts/respond.min.js') }}"></script>
	<![endif]-->
</head>
<body>
 		@section('content')
            This is the master content.
        @show
</body>
</html>