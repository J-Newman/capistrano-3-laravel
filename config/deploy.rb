# config valid only for current version of Capistrano
lock '3.3.5'

set :application, 'laravel5cap3'
set :repo_url, 'git@bitbucket.org:J-Newman/capistrano-3-laravel.git'

# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/home/laravel5cap3/deploy'

# Default value for :scm is :git
set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
set :log_level, :debug

# Default value for :pty is false
set :pty, true

# Default value for :linked_files is []
# set :linked_files, fetch(:linked_files, []).push('config/database.yml')

# Default value for linked_dirs is []
# set :linked_dirs, fetch(:linked_dirs, []).push('bin', 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
set :keep_releases, 3

namespace :composer do
 
    desc "Running Composer Self-Update"
    task :update do
        on roles(:app), in: :sequence, wait: 5 do
            execute :composer, "self-update"
        end
    end
 
    desc "Running Composer Install"
    task :install do
        on roles(:app), in: :sequence, wait: 5 do
        	execute "cd #{release_path}/laravel/ && composer install --no-dev --quiet";
        end
    end
end

namespace :environment do
 
    desc "Set Environment Variables"
    task :setenvfile do
        on roles(:app), in: :sequence, wait: 5 do
        	execute "cd #{release_path}/laravel/ && mv .env.#{fetch(:envfile)} .env";
        end
    end
 
end

namespace :laravel do
 
    desc "Setup Laravel folder permissions"
    task :permissions do
        on roles(:app), in: :sequence, wait: 5 do
            within release_path  do
                execute :chmod, "u+x laravel/artisan"
                execute :chmod, "-R 777 laravel/storage/logs"
                execute :chmod, "-R 777 laravel/storage/framework/cache"
                execute :chmod, "-R 777 laravel/storage/framework/sessions"
                execute :chmod, "-R 777 laravel/storage/framework/views"
            end
        end
    end
 
    desc "Run Laravel Artisan migrate task."
    task :migrate do
        on roles(:app), in: :sequence, wait: 5 do
            within release_path  do
                execute :php, "laravel/artisan migrate"
            end
        end
    end
 
    desc "Run Laravel Artisan seed task."
    task :seed do
        on roles(:app), in: :sequence, wait: 5 do
            within release_path  do
                execute :php, "laravel/artisan db:seed"
            end
        end
    end
 
    desc "Optimize Laravel Class Loader"
    task :optimize do
        on roles(:app), in: :sequence, wait: 5 do
            within release_path  do
                execute :php, "laravel/artisan clear-compiled"
                execute :php, "laravel/artisan optimize"
            end
        end
    end
 
end

namespace :deploy do
	before :publishing, "environment:setenvfile"
	# after :published, "composer:update"
	before :publishing, "composer:install"
	before :publishing, "laravel:permissions"
	before :publishing, "laravel:optimize"
	# before :publishing, "laravel:migrate"
end
